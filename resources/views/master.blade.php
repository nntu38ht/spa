<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Trung Tâm Chăm Sóc Dưỡng Sinh An Tâm</title>
    <link rel="stylesheet" href="./resource/css/style.css">
    <link rel="stylesheet" href="./resource/css/queries.css">
    <link rel="stylesheet" href="./resource/css/animation.css">
    <link rel="stylesheet" href="./resource/css/style-all.css">
    <!-- bootstrap 4 -->
    <link rel="stylesheet" href="./vendor/css/bootstrap.min.css">
    <!-- fontawesome -->
    <link rel="stylesheet" href="./vendor/fonts/fontawesome-5.0.13/css/all.css">
    <link rel="stylesheet" href="./vendor/fonts/natuspa-font/css/natuspa-icons.css">
    <!-- slick -->
    <link rel="stylesheet" href="./vendor/js/slick/slick.css">
    <link rel="stylesheet" href="./vendor/js/slick/slick-theme.css">
    <!-- <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" /> -->
    <link rel="stylesheet" href="./vendor/js/datetimepicker-master/jquery.datetimepicker.css">
    <!-- magnific popup -->
    <link rel="stylesheet" href="./vendor/js/Magnific-Popup-master/magnific-popup.css">
    <!-- favicons -->
    <link rel="apple-touch-icon" sizes="76x76" href="/resource/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/resource/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/resource/favicons/favicon-16x16.png">
    <link rel="manifest" href="/resource/favicons/site.webmanifest">
    <link rel="mask-icon" href="/resource/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="/resource/favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="/resource/favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
</head>

<body class="preload">

<!-- Loading ......... -->
<div class="loader" id="loader">
    <div class="loader-inner">
        <img src="./imgs/loader.svg" alt="Loader" class="loader-img">
    </div>
</div>

<!-- overlay for focus -->
<div id="overlay" style="display: none;"></div>

<!-- scroll top -->
<a class="scroll-top link-text" href="#home">Top</a>

<!-- page wrapper -->
<div class="page-wrapper">
    <!-- Header -->
    <header id="home">
        <!-- Navigation -->
        <nav class="navbar navbar-expand-xl navbar-dark">
            <!-- logo site -->
            <a class="navbar-brand main-logo" href="index.html" rel="home" title="Natuspa">
                <img src="./imgs/logo_spa2.png" alt="Natuspa Logo" width="193" height="76">
            </a>
            <!--  main nav -->
            <button class="navbar-toggler mobile-nav-icon" type="button" data-toggle="collapse"
                    data-target="#main-nav-mobile">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse main-nav">
                <ul class="navbar-nav menu" id="mainNav">
                    <li class="nav-item menu-item menu-item-current">
                        <a class="nav-link" href="/">Trang Chủ</a>
                    </li>
                    <li class="nav-item menu-item">
                        <a class="nav-link" href="service">Dịch Vụ</a>
                    </li>
                    <li class="nav-item menu-item">
                        <a class="nav-link" href="book">Đặt Lịch</a>
                    </li>
                    <li class="nav-item menu-item">
                        <a class="nav-link" href="contact">Liên Hệ</a>
                    </li>
                    <li class="nav-item menu-item">
                        <a class="nav-link" href="about">Giới Thiệu</a>
                    </li>
                </ul>
            </div>
            <!-- nav icon -->
            <div class="main-nav-icon">
                <!-- bars icon -->
                <a class="slide-icon">
                    <span class="bars-icon nav-icon"><i class="fal fa-bars"></i></span>
                </a>
            </div>
        </nav>
        <!-- menu popup -->
        <div class="menu-popup">
            <span class="close-icon"><i class="fal fa-times"></i></span>
            <div class="content-wrap">
                <h1 class="title footer-a-hover">TRUNG TÂM CHĂM SÓC DƯỠNG SINH AN TÂM</h1>
                <p class="text">
                    Đối với chúng tôi, đó là một hành trình, không phải là một cuộc chạy đua để xây dựng di sản và danh tiếng là người giỏi nhất,
                    mang đến những điều tốt nhất và tạo ra một nền văn hóa Dưỡng Sinh mà chúng tôi tự hào mỗi ngày.
                </p>
                <span class="menu-popup-img"><img src="./imgs/summer-sale.jpg" alt="Sale"></span>
                <div class="contact-popup-info">
                    <h2 class="title">Liên Hệ</h2>
                    <div class="address">
                        <span>B206 Đường N1, KĐT Thinh Gia, Tân Định, Bến Cát</span>
                    </div>
                    <div class="email">
                        <span>spaduongsinhantam@gmail.com</span>
                    </div>
                    <div class="phone">
                        <span>0365-908-813</span>
                    </div>
                    <div class="social-media-icons">
                        <div class="social-media">
                            <a class="social-popup-icon" target="_blank" href="#"><i
                                    class="fab fa-linkedin-in"></i></a>
                        </div>
                        <div class="social-media">
                            <a class="social-popup-icon" target="_blank" href="#"><i
                                    class="fab fa-facebook-f"></i></a>
                        </div>
                        <div class="social-media">
                            <a class="social-popup-icon" target="_blank" href="#"><i
                                    class="fab fa-twitter"></i></i></a>
                        </div>
                        <div class="social-media">
                            <a class="social-popup-icon" target="_blank" href="#"><i
                                    class="fab fa-google-plus-g"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- slides -->
        <div class="slide-wrap">
            <div class="slide-inner">
                <div class="slide-item active">
                    <img src="./imgs/slide_3.jpg" alt="slide one">
                    <div class="slide-text">
                        <h3>Chào Mừng Bạn Đến Với Trung Tâm Dưỡng Sinh An Tâm!</h3>
                        <h1>DỊCH VỤ TỐT NHẤT</h1>
                        <p>Bạn xứng đáng được nghỉ ngơi. khám phá lại ý nghĩa của việc thoải mái, cảm thấy tốt hơn và tận hưởng những lợi ích lâu dài tại Trung Tâm.
                        </p>
                        <button onclick="window.location.href='contact'" href="contact" class="btn book-now-btn">Liên Hệ Ngay</button>
                    </div>
                </div>
                <div class="slide-item">
                    <img src="./imgs/slide_2.jpg" alt="slide two">
                    <div class="slide-text" style="text-align: left;">
                        <h3>Chào Mừng Bạn Đến Với Trung Tâm Dưỡng Sinh An Tâm!</h3>
                        <h1>DỊCH VỤ TỐT NHẤT</h1>
                        <p>Bạn xứng đáng được nghỉ ngơi. khám phá lại ý nghĩa của việc thoải mái, cảm thấy tốt hơn và tận hưởng những lợi ích lâu dài tại Trung Tâm.
                        </p>
                        <button onclick="window.location.href='contact'" class="btn book-now-btn">Liên Hệ Ngay</button>
                    </div>
                </div>
                <div class="slide-item">
                    <img src="./imgs/slide_3.jpg" alt="slide three">
                    <div class="slide-text" style="text-align: right;">
                        <h3>Chào Mừng Bạn Đến Với Trung Tâm Dưỡng Sinh An Tâm!</h3>
                        <h1>DỊCH VỤ TỐT NHẤT</h1>
                        <p>Bạn xứng đáng được nghỉ ngơi. khám phá lại ý nghĩa của việc thoải mái, cảm thấy tốt hơn và tận hưởng những lợi ích lâu dài tại Trung Tâm Dưỡng Sinh.
                        </p>
                        <button onclick="window.location.href='contact'" class="btn book-now-btn">Liên Hệ Ngay</button>
                    </div>
                </div>
            </div>
            <a href="#" class="slide-control" id="prev-slide"></a>
            <a href="#" class="slide-control" id="next-slide"></a>
            <ol class="slide-dot">
                <li class="active"></li>
                <li></li>
                <li></li>
            </ol>
        </div>
    </header>

    <!-- content -->
    @yield('content')

</div>
<!-- footer -->
<footer id="footer">
    <div class="footer-content">
        <a href="/" class="footer-a">TRUNG TÂM CHĂM SÓC DƯỠNG SINH AN TÂM</a>
        <div class="sub-heading">Các Trang Truyền Thông</div>
        <div class="slogan">Truy Cập Và Đăng Ký Ngay Để Biết Thêm Nhiều Thông Tin</div>
        <ul class="footer-social">
            <li class="social-media" style="background-color: #4b64a6;">
                <a class="social-popup-icon" target="_blank" href="https://www.facebook.com/spaduongsinhantam">
                    <i class="fab fa-facebook-f"></i>
                </a>
            </li>
            <li class="social-media" style="background-color: red;">
                <a class="social-popup-icon" target="_blank" href="https://www.youtube.com/@spaduongsinhantam">
                    <i class="fab fa-brands fa-youtube"></i>
                </a>
            </li>
            <li class="social-media" style="background-color: white;">
                <a class="social-popup-icon" target="_blank" href="https://www.tiktok.com/@duongsinhantam">
                    <img src="images/3046121.png" alt="" style="width: 25px;">
                </a>
            </li>
        </ul>
    </div>
    <div class="footer-bottom">🏠 Địa chỉ : B206 Đường N1 KĐT Thịnh Gia, Tân Định, Bến Cát, Bình Dương</div>
</footer>

<!-- modal dialog box popup -->
<div class="modal-dialog-box" style="display: none;">
    <div class="inner">
        <div class="dialog-img"><img src="./imgs/popup-img.jpg" alt="Dialog Img" style="height: 410px;"></div>
        <span class="close-icon"><i class="fal fa-times"></i></span>
        <div class="content">
            <h5 class="pre-heading">Get Our Most</h5>
            <h3 class="heading">Exclusive Offers</h3>
            <p class="sub-heading">Get up to 60% Extra Off Your First Deal Use code WELCOME at checkout for 20%
                Extra
                Off Local & Getaways, or 10% Extra Off Goods.
            </p>
            <form action="#">
                <input type="text" placeholder="Your E-mail">
                <button class="btn book-now-btn">Subcriber</button>
            </form>
        </div>
    </div>
</div>
<script>
    function about() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 200;
    }
    function contact() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 200;
    }
    function service() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 200;
    }
    function book() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 200;
    }
    function home() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 200;
    }
</script>
<script src="./vendor/js/jquery-v3.4.1.min.js"></script>
<script src="./vendor/js/bootstrap.min.js"></script>
<script src="./vendor/js/jquery.waypoints.min.js"></script>
<!-- <script src="https://kit.fontawesome.com/7534282567.js" crossorigin="anonymous"></script> -->
<script src="./vendor/js/slick/slick.min.js"></script>
<!-- <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script> -->
<script src="./vendor/js/datetimepicker-master/build/jquery.datetimepicker.full.js"></script>
<script src="./vendor/js/Magnific-Popup-master/jquery.magnific-popup.min.js"></script>

<script src="./resource/js/script.js"></script>
</body>

</html>

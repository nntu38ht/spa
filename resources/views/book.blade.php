@extends('master')
@section('content')
    <!-- appointment & form section -->
    <div class="appointment-section container-fluid">
        <div class="container">
            <div class="row">
                <div class="spacer-clearfix" style="height: 90px;"></div>
                <div class="heading col-12">
                    <div class="pre-heading">Đặt lịch hẹn</div>
                    <div class="sub-heading">Với Khách Hàng Từ 35 Tuổi <br>Đặt Lịch Ngay Trên Website</div>
                    <div class="sub-heading">Giảm giá 35% trên Toàn Hóa Đơn</div>
                </div>
                <div class="spacer-clearfix" style="height: 60px;"></div>
                <div class="appointment-form col-12">
                    <form action="/sendmail-book" method="post">
                        @csrf
                        <div class="row">
                            <div class="form-input col-lg-4 col-md-6 col-sm-12 your-name">
                                <input type="text" name="name" value="{{ old('name')}}" id="name" placeholder="Họ Và Tên">
                                @if($errors->has('name'))
                                    <p class="text-validate">{{ $errors->first('name') }}</p>
                                @endif
                            </div>
                            <div class="form-input col-lg-4 col-md-6 col-sm-12 your-email">
                                <input type="text" name="email" value="{{ old('email')}}" id="email" placeholder="E-mail">
                                @if($errors->has('email'))
                                    <p class="text-validate">{{ $errors->first('email') }}</p>
                                @endif
                            </div>
                            <div class="form-input col-lg-4 col-md-6 col-sm-12 your-phone-number">
                                <input type="text" name="phone_number" value="{{ old('phone_number')}}" id="phone-number" placeholder="Số Điện Thoại">
                                @if($errors->has('phone_number'))
                                    <p class="text-validate">{{ $errors->first('phone_number') }}</p>
                                @endif
                            </div>
                            <div class="form-input col-lg-4 col-md-6 col-sm-12 preferred-date">
                                <input type="text" name="date_time" value="{{ old('date_time')}}" id="datepicker"
                                       placeholder="Thời Gian" autocomplete="off">
                                @if($errors->has('date_time'))
                                    <p class="text-validate">{{ $errors->first('date_time') }}</p>
                                @endif
                            </div>
                            <div class="form-input col-lg-4 col-md-6 col-sm-12 your-service">
                                <select class="current-option" name="service">
                                    @foreach($service as $key=>$sv)
                                        <option class="option" @if( old('service') == $key) selected @endif value="{{$key}}">{{$sv}}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('service'))
                                    <p class="text-validate">{{ $errors->first('service') }}</p>
                                @endif
                            </div>
                            <div class="form-input col-lg-4 col-md-6 col-sm-12 submit-btn">
                                <button type="submit"
                                        class="btn book-now-btn" style="opacity: 1;">Đăng Ký</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="spacer-clearfix" style="height: 80px;"></div>
            </div>
        </div>
    </div>
@endsection

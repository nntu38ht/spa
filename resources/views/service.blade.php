@extends('master')
@section('content')
    <!-- service section -->
    <div class="services-section container" id="services">
        <div class="row">
            <div class="spacer-clearfix" style="height: 80px;"></div>
            <div class="services-info col-md-12">
                <h2 class="section-title">Dịch vụ tốt nhất của chúng tôi</h2>
                <p class="services-main-text">Trung Tâm Chăm Sóc Dưỡng Sinh An Tâm đầy đủ dịch vụ với các chuyên gia về dịch vụ cơ thể, các vấn đề về vai gáy, chăm sóc da và các phương pháp điều trị khác.
                </p>
            </div>
            <div class="service col-lg-4 col-md-6 col-sm-12">
                <div class="services-icon-wrap">
                    <i class="natuspa-icon-facial-treatment"></i>
                </div>
                <div class="heading text-linear-effect">Gội Đầu Thảo Dược</div>
                <div class="description">Là một phương pháp chăm sóc tóc tự nhiên có từ rất lâu thông qua các nguyên liệu,
                    bài thuốc hoàn toàn từ thiên nhiên, không chất hóa học,
                    được đánh giá tốt cho tóc, sức khỏe và cả tinh thần của chúng ta.
                </div>
                <div class="spacer-clearfix" style="height: 50px;"></div>
            </div>
            <div class="service col-lg-4 col-md-6 col-sm-12">
                <div class="services-icon-wrap">
                    <i class="natuspa-icon-depilation"></i>
                </div>
                <div class="heading text-linear-effect">Gội Đầu Dưỡng Sinh</div>
                <div class="description">Là một liệu pháp chăm sóc tóc và da đầu đặc biệt kết hợp massage trên đầu, vai, cổ, lưng và hơn thế nữa.
                    Chuyên viên chăm sóc bấm huyệt, day, xoa bóp …
                    sau đó làm sạch da đầu và tóc bằng các loại dầu gội thảo dược thiên nhiên như sả, bồ kết, vỏ bưởi, quế, …
                </div>
                <div class="spacer-clearfix" style="height: 50px;"></div>
            </div>
            <div class="service col-lg-4 col-md-6 col-sm-12">
                <div class="services-icon-wrap">
                    <i class="natuspa-icon-massage-1"></i>
                </div>
                <div class="heading text-linear-effect">Massage Foot</div>
                <div class="description">Foot massage (hay còn được gọi là massage chân) từ lâu đã được xem là liệu pháp chăm sóc sức khỏe hiệu quả.
                    Phương pháp này được thực hiện bằng cách massage và bấm huyệt dưới lòng bàn chân.
                    Massage foot tác động đến các mạch máu, cơ và dây thần kinh, điểm huyệt vị dưới bàn chân.
                </div>
                <div class="spacer-clearfix" style="height: 50px;"></div>
            </div>
            <div class="service col-lg-4 col-md-6 col-sm-12">
                <div class="services-icon-wrap">
                    <i class="natuspa-icon-herbal"></i>
                </div>
                <div class="heading text-linear-effect">Massage Body</div>
                <div class="description">Là phương pháp thường được sử dụng giúp cơ thể thư giãn, xua tan mệt mỏi.
                    Cách này sử dụng tay hoặc các thiết bị chuyên dụng để tác động vật lý lên các cơ, xương.
                    Ngoài ra, tại một số spa còn kết hợp đá nóng hay túi thảo dược để tăng cường hiệu quả.
                </div>
                <div class="spacer-clearfix" style="height: 0px;"></div>
            </div>
            <div class="service col-lg-4 col-md-6 col-sm-12">
                <div class="services-icon-wrap">
                    <i class="natuspa-icon-polish"></i>
                </div>
                <div class="heading text-linear-effect">Xong Chân Thảo Dược Bài Đọc</div>
                <div class="description">Ngâm chân thảo dược là phương pháp sử dụng nước nóng kết hợp với các loại thảo dược tự nhiên để ngâm chân.
                    Nhờ sự nóng lên của nhiệt độ, các lỗ chân lông ở bàn chân sẽ nở ra, tạo điều kiện tối đa cho các loại thảo dược thẩm thấu...
                </div>
                <div class="spacer-clearfix" style="height: 0px;"></div>
            </div>
            <div class="service col-lg-4 col-md-6 col-sm-12">
                <div class="services-icon-wrap">
                    <i class="natuspa-icon-stone"></i>
                </div>
                <div class="heading text-linear-effect">Massage Mặt Sảng Khoái</div>
                <div class="description">Là một liệu pháp nâng cơ mặt tự nhiên, sử dụng đá ngọc thạch massage kết hợp nhấn huyệt,
                    đánh thức những tế bào già cỗi, khai thông các đường kinh lạc bị tắt nghẽn,
                    cải thiện sự tuần hoàn máu để đưa oxy và dưỡng chất tới từng tế bào.
                </div>
                <div class="spacer-clearfix" style="height: 0px;"></div>
            </div>
            <div class="spacer-clearfix" style="height: 80px;"></div>
        </div>
    </div>
@endsection

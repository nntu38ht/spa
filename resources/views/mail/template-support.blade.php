<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Trung Tâm Chăm Sóc Dưỡng Sinh An Tâm</title>
</head>
<body style="font-style: italic;">
<p style="font-size: 20px"><b>Trung Tâm Chăm Sóc Dưỡng Sinh An Tâm</b> Có Lịch Hẹn Mới </p>
<p style="font-size: 16px">Số điện thoại : <b>{{$phone_number}}</b></p>
<p style="font-size: 16px">Email : <b>{{$email}}</b></p>
<p style="font-size: 16px">Dịch vụ :<b>{{$service}}</b></p>
<p style="font-size: 16px">Nội Dung : <br><b>{{ $hi }}</b></p>
<p style="font-size: 16px">Chúc bạn một ngày làm viêc thật vui vẻ nhé :D </p>
<b style="font-size: 18px">Trân Trọng!</b>
</body>
</html>

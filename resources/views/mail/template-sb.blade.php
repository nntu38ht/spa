<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Trung Tâm Chăm Sóc Dưỡng Sinh An Tâm</title>
</head>
<body style="font-style: italic;">
<p style="font-size: 20px"><b>Trung Tâm Chăm Sóc Dưỡng Sinh An Tâm</b> Cảm ơn quý khách <b>{{$name}}</b> đã quan tâm dịch vụ.</p>
<p style="font-size: 16px">Thắc Mắc Của Quý Khách Sẽ Được Giải Đáp Trong Thời Gian Ngắn Nhất! </p>
<p style="font-size: 16px">Chúc quý Khách một ngày thật vui vẻ và tràn đầy năng lương! </p>
<b style="font-size: 18px">Trân Trọng!</b>
</body>
</html>

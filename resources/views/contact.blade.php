@extends('master')
@section('content')
    <!-- maps section -->
    <div class="maps container-fluid">
        <div class="spacer-clearfix" style="height: 80px;"></div>
        <div class="row">
            <div class="maps-embed col-md-12 col-lg-6 p-0">
                {{--                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d413.8136544669383!2d106.6248937648626!3d11.070238197194643!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1svi!2s!4v1678331150171!5m2!1svi!2s" width="100%" height="100%" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>--}}
                {{--                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d413.8136544669383!2d106.6248937648626!3d11.070238197194643!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1svi!2s!4v1678331150171!5m2!1svi!2s" width="100%" height="100%" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>--}}
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d413.8136544669383!2d106.6248937648626!3d11.070238197194643!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1svi!2s!4v1678331150171!5m2!1svi!2s" width="100%" height="100%" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
            </div>
            <div class="maps-contact col-md-12 col-lg-6">
                <div class="spacer-clearfix" style="height: 80px;"></div>
                <div class="inner">
                    <div class="row">
                        <div class="contact col-lg-12 col-xl-6">
                            <div class="contact-popup-info">
                                <h2 class="section-title">Văn Phòng Chính</h2>
                                <div class="address">
                                    <span>215 Pham Đăng Lưu, Hiệp An</span>
                                </div>
                                <div class="email">
                                    <span>spaduongsinhantam@gmail.com</span>
                                </div>
                                <div class="phone">
                                    <span>0365-908-813</span>
                                </div>
                            </div>
                        </div>
                        <div class="contact col-lg-12 col-xl-6">
                            <div class="contact-popup-info">
                                <h2 class="section-title">Địa Chỉ Chi Nhánh</h2>
                                <div class="address">
                                    <span>B206 Đường N1, KĐT Thinh Gia, Tân Định, Bến Cát</span>
                                </div>
                                <div class="email">
                                    <span>spaduongsinhantam@gmail.com</span>
                                </div>
                                <div class="phone">
                                    <span>0365-908-813</span>
                                </div>
                            </div>
                        </div>
                        <div class="contact col-lg-12">
                            <div class="contact-popup-info">
                                <h2 class="section-title">Nhập Thông Tin Để Được Tư Vấn</h2>
                            </div>
                        </div>
                        <form action="/sendmail-support" method="post">
                            @csrf
                            <div class="row">
                                <div class="form-input col-sm-6 your-name">
                                    <input type="text" name="name" value="{{ old('name')}}" id="name" placeholder="Họ Và Tên">
                                    @if($errors->has('name'))
                                        <p class="text-validate">{{ $errors->first('name') }}</p>
                                    @endif
                                </div>
                                <div class="form-input col-sm-6 your-email">
                                    <input type="text" name="email" value="{{ old('email')}}" id="email" placeholder="E-mail">
                                    @if($errors->has('email'))
                                        <p class="text-validate">{{ $errors->first('email') }}</p>
                                    @endif
                                </div>
                                <div class="form-input col-sm-6 your-phone-number">
                                    <input type="number" name="phone_number" value="{{ old('phone_number')}}" id="phone-number" placeholder="Số Điện Thoại">
                                    @if($errors->has('phone_number'))
                                        <p class="text-validate">{{ $errors->first('phone_number') }}</p>
                                    @endif
                                </div>
                                <div class="form-input col-sm-6 your-service">
                                    <select class="current-option" name="service">
                                        @foreach($service as $key=>$sv)
                                            <option class="option" @if( old('service') == $key) selected @endif value="{{$key}}">{{$sv}}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('service'))
                                        <p class="text-validate">{{ $errors->first('service') }}</p>
                                    @endif
                                </div>
                                <textarea name="message"
                                          placeholder="Ghi Chú">{{old('message')}}</textarea>
                                @if($errors->has('message'))
                                    <p class="text-validate">{{ $errors->first('message') }}</p>
                                @endif
                                <div class="form-input col-12 submit-btn">
                                    <button type="submit"
                                            class="btn book-now-btn"  style="opacity: 1;margin-bottom: 5px">Gửi</button>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

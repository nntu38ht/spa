@extends('master')
@section('content')
    <!-- about-section -->
    <div class="about-section margin-top-head container-fluid" id="about">
        <div class="row">
            <div class="video-box col-lg-6 col-md-12">
                <div class="play-btn">
                    <i class="fas fa-play"></i>
                </div>
            </div>
            <div class="about-main-text col-lg-6 col-md-12">
                <h2 class="section-title">Về Chúng Tôi !</h2>
                <div class="sub-heading">
                    <em>Trung Tâm Chăm Sóc Dưỡng Sinh An Tâm mở cửa với sứ mệnh và tầm nhìn tạo ra sự khác biệt tích cực trong thế giới.</em>
                </div>
                <p class="text-box">Vừa ra mắt vào ngày 21 tháng 9 năm 2022,
                    Trung Tâm Chăm Sóc Dưỡng Sinh An Tâm mở cửa với sứ mệnh và tầm nhìn tạo ra sự khác biệt tích cực trên thế giới với từng vị khách
                    và mang đến sự đặc biệt mà những vị khách của chúng tôi không thể sống thiếu. đối với chúng tôi,
                    đó là một hành trình, không phải là một cuộc chạy đua để xây dựng di sản và danh tiếng là người giỏi nhất,
                    mang đến những điều tốt nhất và tạo ra một nền văn hóa spa mà chúng tôi tự hào mỗi ngày.
                    chúng tôi khiêm tốn và biết ơn vì đã có cơ hội mở cửa mỗi ngày, tạo ra những trải nghiệm đặc biệt và cống hiến cho cộng đồng của chúng tôi...
                </p>
                <a class="link-text text-linear-effect">thank you very much !</a>
            </div>
        </div>
        <iframe src="https://www.youtube.com/embed/bU2CVmqY2Xk" frameborder="0"
                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen
                class="video-embed">
        </iframe>
        <span class="close-btn"><i class="fal fa-times"></i></span>
        <div class="spacer-clearfix" style="height: 80px;"></div>
    </div>
@endsection

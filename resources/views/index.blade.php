@extends('master')
@section('content')
@if(Session::has('message'))
    <p  class="alert alert-success text-center" role="alert" style="font-size: 25px">{{Session::get('message')}}</p>
@endif
@if($errors->has('error'))
    <p  class="alert alert-danger text-center text-validate" role="alert" style="font-size: 25px">{{ $errors->first('error') }}</p>
@endif
    <!-- service section -->
    <div class="services-section container" id="services">
        <div class="row">
            <div class="spacer-clearfix" style="height: 80px;"></div>
            <div class="services-info col-md-12">
                <h2 class="section-title">Dịch vụ tốt nhất của chúng tôi</h2>
                <p class="services-main-text">Trung Tâm Chăm Sóc Dưỡng Sinh An Tâm đầy đủ dịch vụ với các chuyên gia về dịch vụ cơ thể, các vấn đề về vai gáy, chăm sóc da và các phương pháp điều trị khác.
                </p>
            </div>
            <div class="service col-lg-4 col-md-6 col-sm-12">
                <div class="services-icon-wrap">
                    <i class="natuspa-icon-facial-treatment"></i>
                </div>
                <div class="heading text-linear-effect">Gội Đầu Thảo Dược</div>
                <div class="description">Là một phương pháp chăm sóc tóc tự nhiên có từ rất lâu thông qua các nguyên liệu,
                    bài thuốc hoàn toàn từ thiên nhiên, không chất hóa học,
                    được đánh giá tốt cho tóc, sức khỏe và cả tinh thần của chúng ta.
                </div>
                <div class="spacer-clearfix" style="height: 50px;"></div>
            </div>
            <div class="service col-lg-4 col-md-6 col-sm-12">
                <div class="services-icon-wrap">
                    <i class="natuspa-icon-depilation"></i>
                </div>
                <div class="heading text-linear-effect">Gội Đầu Dưỡng Sinh</div>
                <div class="description">Là một liệu pháp chăm sóc tóc và da đầu đặc biệt kết hợp massage trên đầu, vai, cổ, lưng và hơn thế nữa.
                    Chuyên viên chăm sóc bấm huyệt, day, xoa bóp …
                    sau đó làm sạch da đầu và tóc bằng các loại dầu gội thảo dược thiên nhiên như sả, bồ kết, vỏ bưởi, quế, …
                </div>
                <div class="spacer-clearfix" style="height: 50px;"></div>
            </div>
            <div class="service col-lg-4 col-md-6 col-sm-12">
                <div class="services-icon-wrap">
                    <i class="natuspa-icon-massage-1"></i>
                </div>
                <div class="heading text-linear-effect">Massage Foot</div>
                <div class="description">Foot massage (hay còn được gọi là massage chân) từ lâu đã được xem là liệu pháp chăm sóc sức khỏe hiệu quả.
                    Phương pháp này được thực hiện bằng cách massage và bấm huyệt dưới lòng bàn chân.
                    Massage foot tác động đến các mạch máu, cơ và dây thần kinh, điểm huyệt vị dưới bàn chân.
                </div>
                <div class="spacer-clearfix" style="height: 50px;"></div>
            </div>
            <div class="service col-lg-4 col-md-6 col-sm-12">
                <div class="services-icon-wrap">
                    <i class="natuspa-icon-herbal"></i>
                </div>
                <div class="heading text-linear-effect">Massage Body</div>
                <div class="description">Là phương pháp thường được sử dụng giúp cơ thể thư giãn, xua tan mệt mỏi.
                    Cách này sử dụng tay hoặc các thiết bị chuyên dụng để tác động vật lý lên các cơ, xương.
                    Ngoài ra, tại một số spa còn kết hợp đá nóng hay túi thảo dược để tăng cường hiệu quả.
                </div>
                <div class="spacer-clearfix" style="height: 0px;"></div>
            </div>
            <div class="service col-lg-4 col-md-6 col-sm-12">
                <div class="services-icon-wrap">
                    <i class="natuspa-icon-polish"></i>
                </div>
                <div class="heading text-linear-effect">Xong Chân Thảo Dược Bài Đọc</div>
                <div class="description">Ngâm chân thảo dược là phương pháp sử dụng nước nóng kết hợp với các loại thảo dược tự nhiên để ngâm chân.
                    Nhờ sự nóng lên của nhiệt độ, các lỗ chân lông ở bàn chân sẽ nở ra, tạo điều kiện tối đa cho các loại thảo dược thẩm thấu...
                </div>
                <div class="spacer-clearfix" style="height: 0px;"></div>
            </div>
            <div class="service col-lg-4 col-md-6 col-sm-12">
                <div class="services-icon-wrap">
                    <i class="natuspa-icon-stone"></i>
                </div>
                <div class="heading text-linear-effect">Massage Mặt Sảng Khoái</div>
                <div class="description">Là một liệu pháp nâng cơ mặt tự nhiên, sử dụng đá ngọc thạch massage kết hợp nhấn huyệt,
                    đánh thức những tế bào già cỗi, khai thông các đường kinh lạc bị tắt nghẽn,
                    cải thiện sự tuần hoàn máu để đưa oxy và dưỡng chất tới từng tế bào.
                </div>
                <div class="spacer-clearfix" style="height: 0px;"></div>
            </div>
            <div class="spacer-clearfix" style="height: 80px;"></div>
        </div>
    </div>

    <!-- about-section -->
    <div class="about-section margin-top-head container-fluid" id="about">
        <div class="row">
            <div class="video-box col-lg-6 col-md-12">
                <div class="play-btn">
                    <i class="fas fa-play"></i>
                </div>
            </div>
            <div class="about-main-text col-lg-6 col-md-12">
                <h2 class="section-title">Về Chúng Tôi !</h2>
                <div class="sub-heading">
                    <em>Trung Tâm Chăm Sóc Dưỡng Sinh An Tâm mở cửa với sứ mệnh và tầm nhìn tạo ra sự khác biệt tích cực trong thế giới.</em>
                </div>
                <p class="text-box">Vừa ra mắt vào ngày 21 tháng 9 năm 2022,
                    Trung Tâm Chăm Sóc Dưỡng Sinh An Tâm mở cửa với sứ mệnh và tầm nhìn tạo ra sự khác biệt tích cực trên thế giới với từng vị khách
                    và mang đến sự đặc biệt mà những vị khách của chúng tôi không thể sống thiếu. đối với chúng tôi,
                    đó là một hành trình, không phải là một cuộc chạy đua để xây dựng di sản và danh tiếng là người giỏi nhất,
                    mang đến những điều tốt nhất và tạo ra một nền văn hóa spa mà chúng tôi tự hào mỗi ngày.
                    chúng tôi khiêm tốn và biết ơn vì đã có cơ hội mở cửa mỗi ngày, tạo ra những trải nghiệm đặc biệt và cống hiến cho cộng đồng của chúng tôi...
                </p>
                <a class="link-text text-linear-effect">thank you very much !</a>
            </div>
        </div>
        <iframe src="https://www.youtube.com/embed/bU2CVmqY2Xk" frameborder="0"
                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen
                class="video-embed">
        </iframe>
        <span class="close-btn"><i class="fal fa-times"></i></span>
        <div class="spacer-clearfix" style="height: 80px;"></div>
    </div>

    <!-- testimonial section -->
    <div class="testimonial-section container" id="testimonial">
        <div class="section-title">Khách hàng nói về chúng tôi</div>
        <div class="spacer-clearfix" style="height: 35px;"></div>
        <blockquote>
            <div class="slick-slide-cite">
                <p>
                    Tôi vô tình biết đến trung tâm từ một feedback của người bạn, bạn đấy khen trung tâm nhiều lắm nên mình quyết định đi thử xem sao, trải nghiệm của tôi :
                    Bé nhân viên dễ thương, sản phẩm tốt, nhiệt tình với khách, sạch sẽ, vệ sinh, tôi dùng massage mặt xong thấy da thích lắm
                    Mong cho trung tâm ngày càng phát triển và mở rộng thêm nhiều chi nhánh để nhiều người hơn nữa biết đến.
                    <br>
                    <b>Muốn đẹp khỏe hãy đến đây!</b>
                </p>
                <p>
                    Mình biết đến trung tâm từ một feedback của người đồng nghiệp, thấy nó khen nhiều nên cũng đi thử,
                    nói thì hơi quê nhưng lần đầu tiên biết đi làm mặt là nó như thế nào.
                    ở đây vừa đẹp vừa yên tĩnh lại có các bé nhân viên dễ thương và nhiệt tình nữa.
                    Mong cho trung tâm ngày càng phát triển và mở rộng thêm nhiều chi nhánh để nhiều người hơn nữa biết đến.
                    <br>
                    <b>hãy đến trải nghiệm nhé!</b>
                </p>
                <p>
                    Mình vô tình biết đến trung tâm từ một quảng cáo trên facebook, đợt đấy đang có chương trình khuyến mãi, thấy nhiều
                    ưu đãi và spa sang trong còn rẻ nữa nên mình đi thử xem nó như thế nào.
                    Ở đây vừa đẹp vừa yên tĩnh lại có các bé nhân viên dễ thương và nhiệt tình nữa, dịch vụ thì phải nó là phê chữ ê kéo dài luôn.
                    Mong cho trung tâm ngày càng phát triển và mở rộng thêm nhiều chi nhánh để nhiều người hơn nữa biết đến.
                    <br>
                    <b>hãy đến đây và trải nghiệm nhé!</b>
                </p>
                <p>
                    Tôi vô tình biết đến trung tâm từ một feedback của người bạn, bạn đấy khen trung tâm nhiều lắm nên mình quyết định đi thử xem sao, trải nghiệm của tôi :
                    Bé nhân viên dễ thương, sản phẩm tốt, nhiệt tình với khách, sạch sẽ, vệ sinh, tôi dùng massage mặt xong thấy da thích lắm
                    Mong cho trung tâm ngày càng phát triển và mở rộng thêm nhiều chi nhánh để nhiều người hơn nữa biết đến.
                    <br>
                    <b>Muốn đẹp khỏe hãy đến đây!</b>
                </p>
                <p>
                    Mình biết đến trung tâm từ một feedback của người đồng nghiệp, thấy nó khen nhiều nên cũng đi thử,
                    nói thì hơi quê nhưng lần đầu tiên biết đi làm mặt là nó như thế nào.
                    ở đây vừa đẹp vừa yên tĩnh lại có các bé nhân viên dễ thương và nhiệt tình nữa.
                    Mong cho trung tâm ngày càng phát triển và mở rộng thêm nhiều chi nhánh để nhiều người hơn nữa biết đến.
                    <br>
                    <b>hãy đến trải nghiệm nhé!</b>
                </p>
            </div>
            <div class="slick-slide-figure-img">
                <div><img class="radius-img"  src="{{url('imgs/kh05.jpg')}}" alt="customer"></div>
                <div><img class="radius-img"  src="{{url('imgs/kh03.jpg')}}" alt="customer"></div>
                <div><img class="radius-img"  src="{{url('imgs/kh01.png')}}" alt="customer"></div>
                <div><img class="radius-img"  src="{{url('imgs/kh02.jpg')}}" alt="customer"></div>
                <div><img class="radius-img"  src="{{url('imgs/kh04.jpg')}}" alt="customer"></div>
                <div><img class="radius-img"  src="{{url('imgs/kh02.jpg')}}" alt="customer"></div>
            </div>
            <div class="slick-slide-figure-info">
                <div class="infomation">
                    <div class="name">Cao Mỹ Duyên</div>
                    <div class="position">Khách Hàng</div>
                </div>
                <div class="infomation">
                    <div class="name">Nguyễn Thùy Linh</div>
                    <div class="position">Khách Hàng</div>
                </div>
                <div class="infomation">
                    <div class="name">Lê Thu Thảo</div>
                    <div class="position">Khách Hàng</div>
                </div>
                <div class="infomation">
                    <div class="name">Nguyễn Thị Linh</div>
                    <div class="position">Khách Hàng</div>
                </div>
                <div class="infomation">
                    <div class="name">Trần Cẩm Tú</div>
                    <div class="position">Khách Hàng</div>
                </div>
                <div class="infomation">
                    <div class="name">Nguyễn Thị Mai</div>
                    <div class="position">Khách Hàng</div>
                </div>
            </div>
        </blockquote>
        <div class="spacer-clearfix" style="height: 80px;"></div>
    </div>

    <!-- appointment & form section -->
    <div class="appointment-section container-fluid">
        <div class="container">
            <div class="row">
                <div class="spacer-clearfix" style="height: 90px;"></div>
                <div class="heading col-12">
                    <div class="pre-heading">Đặt lịch hẹn</div>
                    <div class="main-heading">Cuối Tuần<br>Vào Thứ 7 Và Chủ Nhật</div>
                    <div class="sub-heading">Giảm giá 10% trên Toàn Hóa Đơn<br> Tối Đa 100k</div>
                </div>
                <div class="spacer-clearfix" style="height: 60px;"></div>
                <div class="appointment-form col-12">
                    <form action="/sendmail-book" method="post">
                        @csrf
                        <div class="row">
                            <div class="form-input col-lg-4 col-md-6 col-sm-12 your-name">
                                <input type="text" name="name" value="{{ old('name')}}" id="name" placeholder="Họ Và Tên">
                                @if($errors->has('name'))
                                    <p class="text-validate">{{ $errors->first('name') }}</p>
                                @endif
                            </div>
                            <div class="form-input col-lg-4 col-md-6 col-sm-12 your-email">
                                <input type="text" name="email" value="{{ old('email')}}" id="email" placeholder="E-mail">
                                @if($errors->has('email'))
                                    <p class="text-validate">{{ $errors->first('email') }}</p>
                                @endif
                            </div>
                            <div class="form-input col-lg-4 col-md-6 col-sm-12 your-phone-number">
                                <input type="number" name="phone_number" value="{{ old('phone_number')}}" id="phone-number" placeholder="Số Điện Thoại">
                                @if($errors->has('phone_number'))
                                    <p class="text-validate">{{ $errors->first('phone_number') }}</p>
                                @endif
                            </div>
                            <div class="form-input col-lg-4 col-md-6 col-sm-12 preferred-date">
                                <input type="text" name="date_time" value="{{ old('date_time')}}" id="datepicker"
                                       placeholder="Thời Gian" autocomplete="off">
                                @if($errors->has('date_time'))
                                    <p class="text-validate">{{ $errors->first('date_time') }}</p>
                                @endif
                            </div>
                            <div class="form-input col-lg-4 col-md-6 col-sm-12 your-service">
                                <select class="current-option" name="service">
                                    @foreach($service as $key=>$sv)
                                    <option class="option" @if( old('service') == $key) selected @endif value="{{$key}}">{{$sv}}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('service'))
                                    <p class="text-validate">{{ $errors->first('service') }}</p>
                                @endif
                            </div>
                            <div class="form-input col-lg-4 col-md-6 col-sm-12 submit-btn">
                                <button type="submit"
                                        class="btn book-now-btn" style="opacity: 1;">Đăng Ký</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="spacer-clearfix" style="height: 80px;"></div>
            </div>
        </div>
    </div>
    <div class="collection">
        <a href="{{url('imgs/slide-01.jpg')}}" class="collect-item" style="border: inset #f96882;">
            <img src="{{url('imgs/slide-01.jpg')}}" alt="Gallery-1" class="collect-img" id="gallery-1">
            <span class="plus-icon"><i class="fal fa-plus"></i></span>
        </a>
        <a href="{{url('imgs/slide-02.jpg')}}" class="collect-item"style="border: inset #f96882;">
            <img src="{{url('imgs/slide-02.jpg')}}" alt="Gallery-2" class="collect-img" id="gallery-2">
            <span class="plus-icon"><i class="fal fa-plus"></i></span>
        </a>
        <a href="{{url('imgs/slide-03.jpg')}}" class="collect-item"style="border: inset #f96882;">
            <img src="{{url('imgs/slide-03.jpg')}}" alt="Gallery-3" class="collect-img" id="gallery-3">
            <span class="plus-icon"><i class="fal fa-plus"></i></span>
        </a>
        <a href="{{url('imgs/slide-04.jpg')}}" class="collect-item"style="border: inset #f96882;">
            <img src="{{url('imgs/slide-04.jpg')}}" alt="Gallery-4" class="collect-img" id="gallery-4">
            <span class="plus-icon"><i class="fal fa-plus"></i></span>
        </a>
    </div>

    <!-- day spa packages -->
    <div class="spa-packages container-fluid">
        <div class="spacer-clearfix" style="height: 70px;"></div>
        <div class="container">
            <div class="row">
                <div class="heading col-12">
                    <h2 class="main-heading section-title">Các Gói Dịch Vụ Giá Hấp Dẫn</h2>
                    <div class="sub-heading">
                        <p>Chào mừng đến với Trung Tâm Chăm Sóc Dưỡng Sinh An Tâm!
                            Một trung tâm spa chăm sóc sức khỏe sang trọng dành riêng để truyền cảm hứng cho Cuộc sống cân bằng.
                        </p>
                    </div>
                </div>
                <div class="package-box col-lg-4 col-md-6 col-sm-12">
                    <h2 class="section-title name-price-package">Gội Đầu Thảo Dược<br><span class="price">69K</span>
                    </h2>
                    <ul class="features">
                        <li class="feature-item">Thư giãn và giải toả căng thẳng</li>
                        <li class="feature-item">Lưu thông tuần hoàn máu</li>
                        <li class="feature-item">Giúp tóc phát triển khoẻ mạnh</li>
                        <li class="feature-item">Tăng khả năng tập trung cao</li>
                        <li class="feature-item">Đẩy lùi lão hoá và phòng ngừa bệnh tật</li>
                    </ul>
                    <a href="book" class="link-text text-linear-effect purchase-btn">Đặt Lịch Ngay</a>
                </div>
                <div class="package-box center col-lg-4 col-md-6 col-sm-12">
                    <h2 class="section-title name-price-package">Gội Đầu Dưỡng Sinh<br><span class="price">149K</span>
                    </h2>
                    <ul class="features">
                        <li class="feature-item">Cải thiện, chống lão hóa làn da.</li>
                        <li class="feature-item">Cải thiện phần cổ - vai - gáy.</li>
                        <li class="feature-item">Điều trị dứt điểm các bệnh về da đầu.</li>
                        <li class="feature-item">Củng cố sức khỏe, giảm âu lo, mệt mỏi.</li>
                        <li class="feature-item">Ngăn ngừa một số bệnh liên quan đến trí não.</li>
                    </ul>
                    <a href="book" class="link-text text-linear-effect purchase-btn">Đặt Lịch Ngay<</a>
                </div>
                <div class="package-box col-lg-4 col-md-6 col-sm-12">
                    <h2 class="section-title name-price-package">Massage Mặt Sảng Khoái<br><span class="price">149K</span>
                    </h2>
                    <ul class="features">
                        <li class="feature-item">Trẻ hóa da mặt</li>
                        <li class="feature-item">Giảm áp lực xoang</li>
                        <li class="feature-item">Giảm xuất hiện mụn trứng cá</li>
                        <li class="feature-item">Giúp làn da được trắng sáng</li>
                        <li class="feature-item">Chống lão hóa và hạn chế nếp nhăn</li>
                        <li class="feature-item">& Nhiều tác dụng tốt khác</li>
                    </ul>
                    <a href="book" class="link-text text-linear-effect purchase-btn">Đặt Lịch Ngay<</a>
                </div>
            </div>
        </div>
        <div class="spacer-clearfix" style="height: 80px;"></div>
    </div>

    <!-- counter section -->
    <div class="counter-section container-fluid">
        <div class="container">
            <div class="row">
                <div class="heading col-md-12 col-lg-6">
                    <div class="pre-heading">tại sao chọn chúng tôi ?</div>
                    <div class="main-heading">THƯỞNG THỨC<br>SỰ KHÁC BIỆT</div>
                    <div class="sub-heading">
                        <p>Hàng ngày, mỗi cơ sở đều tiếp đón rất nhiều các chị em đến sử dụng dịch vụ dưỡng sinh đông y chăm sóc sức khỏe và làm đẹp. Mỗi liệu trình,
                            phương pháp đều được các chuyên gia nghiên cứu kỹ lưỡng trước khi áp dụng cho từng khách hàng. Đảm bảo phương pháp hiệu quả, an toàn cho các chị em trải nghiệm.
                        </p>
                    </div>
                </div>
                <div class="counter col-md-12 col-lg-6">
                    <div class="counter-item">
                        <div class="number"><span class="price" id="number-1"></span></div>
                        <h3 class="title">KH Hạnh Phúc</h3>
                    </div>
                    <div class="counter-item">
                        <div class="number"><span class="price" id="number-2"></span></div>
                        <h3 class="title">Phương Pháp Điều Trị</h3>
                    </div>
                    <div class="counter-item">
                        <div class="number"><span class="price" id="number-3"></span></div>
                        <h3 class="title">KH VIP </h3>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- maps section -->
    <div class="maps container-fluid">
        <div class="spacer-clearfix" style="height: 80px;"></div>
        <div class="row">
            <div class="maps-embed col-md-12 col-lg-6 p-0">
                {{--                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d413.8136544669383!2d106.6248937648626!3d11.070238197194643!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1svi!2s!4v1678331150171!5m2!1svi!2s" width="100%" height="100%" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>--}}
                {{--                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d413.8136544669383!2d106.6248937648626!3d11.070238197194643!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1svi!2s!4v1678331150171!5m2!1svi!2s" width="100%" height="100%" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>--}}
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d413.8136544669383!2d106.6248937648626!3d11.070238197194643!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1svi!2s!4v1678331150171!5m2!1svi!2s" width="100%" height="100%" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
            </div>
            <div class="maps-contact col-md-12 col-lg-6">
                <div class="spacer-clearfix" style="height: 80px;"></div>
                <div class="inner">
                    <div class="row">
                        <div class="contact col-lg-12 col-xl-6">
                            <div class="contact-popup-info">
                                <h2 class="section-title">Văn Phòng Chính</h2>
                                <div class="address">
                                    <span>215 Pham Đăng Lưu, Hiệp An</span>
                                </div>
                                <div class="email">
                                    <span>spaduongsinhantam@gmail.com</span>
                                </div>
                                <div class="phone">
                                    <span>0365-908-813</span>
                                </div>
                            </div>
                        </div>
                        <div class="contact col-lg-12 col-xl-6">
                            <div class="contact-popup-info">
                                <h2 class="section-title">Địa Chỉ Chi Nhánh</h2>
                                <div class="address">
                                    <span>B206 Đường N1, KĐT Thinh Gia, Tân Định, Bến Cát</span>
                                </div>
                                <div class="email">
                                    <span>spaduongsinhantam@gmail.com</span>
                                </div>
                                <div class="phone">
                                    <span>0365-908-813</span>
                                </div>
                            </div>
                        </div>
                        <div class="contact col-lg-12">
                            <div class="contact-popup-info">
                                <h2 class="section-title">Nhập Thông Tin Để Được Tư Vấn</h2>
                            </div>
                        </div>
                        <form action="/sendmail-support" method="post">
                            @csrf
                            <div class="row">
                                <div class="form-input col-sm-6 your-name">
                                    <input type="text" name="name" value="{{ old('name')}}" id="name" placeholder="Họ Và Tên">
                                    @if($errors->has('name'))
                                        <p class="text-validate">{{ $errors->first('name') }}</p>
                                    @endif
                                </div>
                                <div class="form-input col-sm-6 your-email">
                                    <input type="text" name="email" value="{{ old('email')}}" id="email" placeholder="E-mail">
                                    @if($errors->has('email'))
                                        <p class="text-validate">{{ $errors->first('email') }}</p>
                                    @endif
                                </div>
                                <div class="form-input col-sm-6 your-phone-number">
                                    <input type="number" name="phone_number" value="{{ old('phone_number')}}" id="phone-number" placeholder="Số Điện Thoại">
                                    @if($errors->has('phone_number'))
                                        <p class="text-validate">{{ $errors->first('phone_number') }}</p>
                                    @endif
                                </div>
                                <div class="form-input col-sm-6 your-service">
                                    <select class="current-option" name="service">
                                        @foreach($service as $key=>$sv)
                                            <option class="option" @if( old('service') == $key) selected @endif value="{{$key}}">{{$sv}}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('service'))
                                        <p class="text-validate">{{ $errors->first('service') }}</p>
                                    @endif
                                </div>
                                <textarea name="message"
                                          placeholder="Ghi Chú"></textarea>
                                @if($errors->has('message'))
                                    <p class="text-validate">{{ $errors->first('message') }}</p>
                                @endif
                                <div class="form-input col-12 submit-btn">
                                    <button type="submit"
                                            class="btn book-now-btn"  style="opacity: 1;margin-bottom: 5px">Gửi</button>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<?php

namespace App\Http\Controllers;

use App\Http\Requests\SendMailBookRequest;
use App\Http\Requests\SendMailSupportRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;

class HomeController extends Controller
{
    /**
     * Show the profile for a given user.
     */
    public function index()
    {
        $service = config('constants.service');
        return view("index")->with(compact('service'));
    }

    public function about()
    {
        return view("about");
    }

    public function contact()
    {
        $service = config('constants.service');
        return view("contact")->with(compact('service'));
    }

    public function service()
    {
        return view("service");
    }

    public function book()
    {
        $service = config('constants.service');
        return view("book")->with(compact('service'));
    }

    public function sendMailBook(SendMailBookRequest $req)
    {
        $service = config('constants.service');
        Mail::send('mail.template', ['name' => $req->input('name'), 'phone_number' => $req->input('phone_number'), 'service' => $service[$req->input('service')], 'date_time' => $req->input('date_time')],
            function ($email) use ($req) {
                $email->to($req->input('email'), $req->input('name'))->subject('[XÁC NHẬN] Đặt Lịch Thành Công');
            });
        Mail::send('mail.template-admin', ['name' => $req->input('name'), 'phone_number' => $req->input('phone_number'), 'email' => $req->input('email'), 'service' => $service[$req->input('service')], 'date_time' => $req->input('date_time')],
            function ($email) use ($req) {
                $email->to('spaantam61@gmail.com', 'admin')->subject('[LỊCH HẸN] Có Một Lịch Hẹn Mới '.$req->input('email'));
            });
        $service = config('constants.service');
        return Redirect::route('index')->with('message', 'Thông Tin Đặt Lịch Đã Được Gửi Thành Công ! ');
    }

    public function sendMailSupport(SendMailSupportRequest $req)
    {
        $service = config('constants.service');
        Mail::send('mail.template-sb', ['name' => $req->input('name'), 'phone_number' => $req->input('phone_number'), 'service' => $service[$req->input('service')], 'message' => $req->input('message')],
            function ($email) use ($req) {
                $email->to($req->input('email'), $req->input('name'))->subject('[TƯ VẤN] Vấn Đề Của Quý Khách Sẽ Được Phản Hồi Sớm Nhất ');
            });
        Mail::send('mail.template-support', ['name' => $req->input('name'), 'phone_number' => $req->input('phone_number'), 'email' => $req->input('email'), 'hi' => $req->input('message'), 'service' => $service[$req->input('service')]],
            function ($email) use ($req) {
                $email->to('spaantam61@gmail.com', 'admin')->subject('[SUPPORT] Có Một Thắc Mắc Mới '.$req->input('email'));
            });
        return Redirect::route('index')->with('message', 'Thông Tin Tư Vấn Đã Được Gửi Thành Công ! ');
    }
}

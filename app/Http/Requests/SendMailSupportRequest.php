<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SendMailSupportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => 'required',
            "phone_number" => 'required|numeric|digits:10',
            "service" => 'required|integer',
            "message" => 'required',
            "email" => 'required|email',
        ];
    }

    public function messages()
    {
        return [
            "name.required" => 'Mục Này Là Bắt Buộc !',
            "phone_number.required" => 'Mục Này Là Bắt Buộc !',
            "phone_number.digits" => 'Số Điện Thoại Phải Là 10 Chữ Số !',
            "service.required" => 'Mục Này Là Bắt Buộc !',
            "message.required" => 'Mục Này Là Bắt Buộc !',
            "email.required" => 'Mục Này Là Bắt Buộc !',
            "phone_number.numeric" => 'Số Điện Thoại Chưa Đúng Định Dạng !',
            "email.email" => 'Mục này phải nhập dạng email !',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if ($validator->errors()->count() > 0) {
                $validator->errors()->add('error', 'Thông Tin Tư Vấn Chưa Được Gửi Thành Công!');
            }
        });
    }

}
